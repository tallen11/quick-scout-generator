package com.team4908.qsgenerator;

public class Constants {
	public static final String TEAM_NUMBER_ID = "!";
	public static final String TEAM_NAME_ID = "@";
	public static final int CODE_SIZE = 200;
	public static final String CODE_FILETYPE = "png";
}
