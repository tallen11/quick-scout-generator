package com.team4908.qsgenerator;

public class Entry {

	private String title;
	private String data;
	private String units;

	public Entry(String title, String data, String units) {
		this.title = title;
		this.data = data;
		this.units = units;
	}
	
	public Entry(String title, String data) {
		this.title = title;
		this.data = data;
		this.units = "";
	}

	public String getTitle() {
		return title;
	}

	public String getData() {
		return data;
	}
	
	public String getUnits() {
		return units;
	}
	
	public String export() {
		String data = "[" + getTitle() + "]" + getData();
		if (!getUnits().equals("")) {
			data += "{" + getUnits() + "}";
		}
		
		return data;
	}
}
