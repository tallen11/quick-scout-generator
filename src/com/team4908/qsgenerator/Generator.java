package com.team4908.qsgenerator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class Generator {

	private Scanner console;
	private InfoPackager infoPackager;

	public Generator() {
		console = new Scanner(System.in);
	}

	public void start() {
		System.out.println();
		System.out.println("FRC QuickScout Code Generator v1.0");
		String teamNumber = requestInputNumber("Team number", false);
		String teamName = requestInput("Team name", false);
		System.out.println();

		infoPackager = new InfoPackager(teamNumber, teamName);

		System.out.println("Enter custom data fields. Type done to finish.");
		while (true) {
			String title = requestInput("Field title", false);
			if (title.equals("done"))
				break;

			String data = requestInput("Field data", false);
			if (data.equals("done"))
				break;

			String units = requestInput("Field units (optional)", true);
			if (units.equals("done"))
				break;

			infoPackager.addEntry(title, data, units);
			System.out.println();
		}

		System.out.println();

		String filePath = "";
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int val = fileChooser.showOpenDialog(null);
		if (val == JFileChooser.APPROVE_OPTION)
			filePath = fileChooser.getSelectedFile().getAbsolutePath();
		else {
			System.out.println("No destination selected, quitting...");
			System.exit(0);
		}

		filePath = filePath.trim();
		filePath += "/FRC_CODE.png";
		String codeData = infoPackager.exportToData();

		try {
			exportQRCode(filePath, codeData);
		} catch (WriterException e) {
			System.out.println("ERROR writing data to code!");
		} catch (IOException e) {
			System.out.println("ERROR: File path is invalid!");
		}

		System.out.println("Code successfully saved to: " + filePath);
		System.out.println();
	}

	private void exportQRCode(String path, String data) throws WriterException, IOException {
		HashMap<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<>();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = qrCodeWriter.encode(data, BarcodeFormat.QR_CODE, Constants.CODE_SIZE, Constants.CODE_SIZE, hintMap);

		int codeWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(codeWidth, codeWidth, BufferedImage.TYPE_INT_RGB);
		image.createGraphics();

		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, codeWidth, codeWidth);
		graphics.setColor(Color.BLACK);

		for (int i = 0; i < codeWidth; i++) {
			for (int j = 0; j < codeWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}

		File file = new File(path);
		ImageIO.write(image, Constants.CODE_FILETYPE, file);
	}

	private String requestInput(String prompt, boolean canBlank) {
		while (true) {
			System.out.print("+ " + prompt + ": ");
			String input = console.nextLine();
			if (!canBlank && input.equals(""))
				System.out.println("Input cannot be blank!");
			else
				return input;
		}
	}

	private String requestInputNumber(String prompt, boolean canBlank) {
		while (true) {
			String input = requestInput(prompt, canBlank);
			try {
				Integer.parseInt(input);
			} catch (NumberFormatException e) {
				System.out.println("Enter a valid number");
				continue;
			}

			return input;
		}
	}

	public static void main(String args[]) {
		Generator gen = new Generator();
		gen.start();
	}
}
