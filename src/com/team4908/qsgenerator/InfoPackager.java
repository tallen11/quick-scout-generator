package com.team4908.qsgenerator;

import java.util.ArrayList;

public class InfoPackager {
	
	//TODO: Keep track of data size and warn user if over limit
	
	private ArrayList<Entry> entries;
	
	public InfoPackager(String teamNumber, String teamName) {
		entries = new ArrayList<>();
		entries.add(new Entry(Constants.TEAM_NUMBER_ID, teamNumber));
		entries.add(new Entry(Constants.TEAM_NAME_ID, teamName));
	}
	
	public void addEntry(String title, String data) {
		//int len = title.length() + data.length() + 2;
		entries.add(new Entry(title, data));
	}
	
	public void addEntry(String title, String data, String units) {
		//int len = title.length() + data.length() + units.length() + 4;
		entries.add(new Entry(title, data, units));
	}
	
	public String exportToData() {
		StringBuilder sb = new StringBuilder();
		for (Entry entry : entries)
			sb.append(entry.export());
		
		return sb.toString();
	}
}
