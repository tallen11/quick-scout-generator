# README #

## Quick Scout Generator Source ##

This tool should be used to generate a Quick Scout readable QR code image to be displayed at FRC competitions for simple and painless scouting.

## How To Get ##

To get this tool, either clone the repository and build it youself, or download the latest stable version from the downloads section to the left.

## How To Use ##

TO DO: Instructions on use
TO DO: Link to Quick Scout

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Author ##
	All code by Tate Allen, FRC team 4908